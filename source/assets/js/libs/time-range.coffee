class TimeRange
  buildRange: (ampm) ->
    range = [1..12].map (number) -> number + ampm
    lastHour = range.pop()
    range.unshift lastHour
    range

  range: ->
    amRange = @buildRange("am")
    pmRange = @buildRange("pm")
    amRange.concat pmRange

@TimeRange = TimeRange
