class MonthGenerator
  constructor: ->
    @id = 1
    @date = new Date()
    @daysInMonth = new Date(@date.getFullYear(), @date.getDay(), 0).getDate()
    @curDay = @date.getDay()
    @curMonth = @date.getMonth()
    @curYear = @date.getFullYear()
    @dayNames = ["Wed", "Thurs", "Fri", "Sat", "Sun", "Mon", "Tues"] # This order would be determined programatically
    @monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    @data = [
      id: @id
      name: @monthNames[@curMonth]
      year: @curYear
      days: [1..@daysInMonth]
      dayNames: @dayNames
    ]

@MonthGenerator = MonthGenerator
