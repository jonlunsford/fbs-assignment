class DayGenerator
  constructor: (dayNumber) ->
    @timeRange = new TimeRange().range()
    @curDate = new Date()
    @daysInMonth = new Date(@curDate.getFullYear(), @curDate.getDay(), 0).getDate()
    @curDayNumber = @curDate.getDate()
    @data = @buildDays([1..@daysInMonth], @curDayNumber, @timeRange)

  buildDays: (days, curDayNumber, timeRange) ->
    days.map (number) => { id: number, curDay: curDayNumber, number: number, time: timeRange, month: [1] }

@DayGenerator = DayGenerator
