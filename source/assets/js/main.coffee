#= require libs/time-range
#= require libs/day-generator
#= require libs/month-generator

# Main App

# Config

App = Ember.Application.create
  LOG_TRANSITIONS: true

App.Store = DS.Store.extend
  adapter: DS.FixtureAdapter

# Routes

App.Router.map ->
  @resource "month", ->
    @route "day", { path: "/day/:day_id" }

App.IndexRoute = Ember.Route.extend
  model: -> @store.find("month", 1)

App.MonthDayRoute = Ember.Route.extend
  model: (params) ->
    @store.find("day", params.day_id)

  setupController: (controller, model) ->
    @_super(controller, model)
    controller.send("cancel")

# Models

App.Month = DS.Model.extend
  days: DS.hasMany "day", { async: true }
  name: DS.attr()
  number: DS.attr()
  year: DS.attr()
  dayOfWeek: DS.attr()
  dayNames: DS.attr()

App.Day = DS.Model.extend
  month: DS.belongsTo "month", { async: true }
  curDay: DS.attr()
  number: DS.attr()
  time: DS.attr()
  isPast: Ember.computed "number", "curDay", ->
    @get("number") < @get("curDay")
  isCur: Ember.computed "number", "curDay", ->
    @get("number") is @get("curDay")

# Views

App.SchedularView = Ember.View.extend
  templateName: "schedular"

App.HoursView = Ember.View.extend
  templateName: "hours"
  click: (event) ->
    @get("controller").send("schedule", event)

App.EventView = Ember.View.extend
  templateName: "event"

# Controllers

App.MonthDayController = Ember.ObjectController.extend
  eventTime: "12am - 1pm"
  eventTitle: ""
  isScheduling: false
  hasScheduledEvent: false
  $currentSlot: ""
  actions:
    schedule: (event) ->
      $timeSlot = $(event.target)
      startTime = $timeSlot.text()

      @set "$currentSlot", $timeSlot
      @set "isScheduling", true
      @set "eventTime", startTime
      @positionSchedular(event, $(".calendar-time--schedular"))
    save: ->
      @positionEvent()
      @set "isScheduling", false
    cancel: ->
      @set "eventTime", ""
      @set "eventTitle", ""
      @set "isScheduling", false
      @set "hasScheduledEvent", false

  positionSchedular: (event, $schedular) ->
    offsetY = event.pageY - 160
    offsetX = event.pageX - ($schedular.width() / 2 - 20)
    $schedular.css
      top: "#{offsetY}px"
      left: "#{offsetX}px"

  positionEvent: ->
    $timeSlot = @get "$currentSlot"
    $event = $(".calendar-time")
    offset = $timeSlot.offset().top - ($timeSlot.outerHeight() * 2) - 5

    @set "hasScheduledEvent", true

    $event.css
      top: "#{offset}px"

# Fixtures

App.Day.FIXTURES = new DayGenerator().data

App.Month.FIXTURES = new MonthGenerator().data

